# Multiple inclusion
ifneq (target.mk,$(filter target.mk,$(notdir $(filter-out $(lastword ${MAKEFILE_LIST}),${MAKEFILE_LIST}))))

%/: ; @mkdir --parent $@
%: %/

SELF := $(basename $(notdir $(lastword ${MAKEFILE_LIST})))

define LF


endef

define new-target =
$(eval \
.PHONY: $1 $1-$(SELF) \
$(LF) \
NEW_TARGETS += $1 $1-$(SELF) \
$(LF) \
$1: $1-$(SELF) \
$(LF) \
$1-$(SELF): ; $2 \
)
endef

define list-targets =
$(shell $(MAKE) -prns | sed -n '/=/d;/%/d;/$$(.*)/d;/^\./d;s/\(^[^#].*\):.*/\1/p')
endef

ifeq ($(ROOTLEVEL),$(MAKELEVEL))
else
endif

ifeq (,$(SRCDIR))
SRCDIR := .
endif

ifeq (n,$(findstring n,${MAKEFLAGS}))
JUST_PRINT := yes
endif

ifndef _MAIN_
ifndef ROOTLEVEL
ROOTLEVEL := $(MAKELEVEL)
export ROOTLEVEL
endif
endif

ifeq ($(ROOTLEVEL),$(MAKELEVEL))
MAKEFILES := $(filter-out $(lastword $(MAKEFILE_LIST)), $(MAKEFILE_LIST))
MAKEFILES := $(realpath $(filter-out $(lastword $(MAKEFILES)), $(MAKEFILES)))

.SUFFIXES:

BUILDDIR_PREFIX ?= build/
BUILDDIR ?= $(BUILDDIR_PREFIX)$(BUILDDIR_SUFFIX)
export BUILDDIR

ifdef JUST_PRINT

_MAIN_ := $(lastword $(MAKEFILES))
$(call new-target,distclean)

else

ifeq ($(realpath $(BUILDDIR)),$(realpath $(SRCDIR)))
_MAIN_ := $(lastword $(MAKEFILES))
$(call new-target,distclean)
else

TARGET := $(or $(filter-out distclean,$(MAKECMDGOALS)), $(.DEFAULT_GOAL))
.PHONY: $(TARGET)
TARGETS = $(call list-targets)
$(info $(TARGETS))
$(TARGETS):

RELATIVEDIR := $(shell realpath --canonicalize-missing --relative-to $(BUILDDIR) $(CURDIR))
$(TARGET): $(BUILDDIR)/
	+@[ ! -d $(BUILDDIR) ] || $(MAKE) -C $(BUILDDIR) $(addprefix -f , $(MAKEFILES)) VPATH=$(RELATIVEDIR) --include-dir $(RELATIVEDIR) $(MAKECMDGOALS)

$(MAKEFILE_LIST): ;
$(call new-target,distclean,$(RM) -r $(BUILDDIR))
endif

endif

else
unexport ROOTLEVEL
_MAIN_ := $(lastword $(MAKEFILES))
endif

endif # Multiple inclusion
